import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LoginComponent } from '../login/login.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { PlainComponent } from '../plain/plain.component';

import { Routes, RouterModule } from '@angular/router';
//import { AuthguardService } from '../service/authguard.service';



const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'plain', component: PlainComponent }
]



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }
